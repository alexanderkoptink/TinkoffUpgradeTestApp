package tests

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.pages.FirstPage
import ru.tinkoff.myupgradeapplication.pages.LoginPage

@RunWith(AndroidJUnit4::class)
class TestNavigationDisplays {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkTextPositionNotSavedOnNavigation() {
        with(FirstPage()) {
            checkFirstTextDisplay(firstText)
            pressChangeButton()
            checkSecondTextDisplay(secondText)
            pressNextButton()
        }
        with(LoginPage()) {
            checkLoginPageDisplay()
            pressPreviousButton()
        }
        with(FirstPage()) {
            checkFirstTextDisplay(firstText)
        }
    }

    @Test
    fun checkLoginAndPasswordNotSavedAfterBackScreen() {
        with(FirstPage()) {
            pressNextButton()
        }
        with(LoginPage()) {
            checkLoginPageDisplay()
            enterLogin(loginValue)
            enterPassword(passwordValue)
            pressPreviousButton()
        }
        with(FirstPage()) {
            pressNextButton()
        }
        with(LoginPage()) {
            checkLoginPageDisplay()
            pressSubmitButton()
            checkLoginAndPasswordCleared(textNoLoginAndPassword)
        }
    }
}

