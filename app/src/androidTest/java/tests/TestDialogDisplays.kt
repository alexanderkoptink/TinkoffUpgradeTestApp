package tests

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.pages.FirstPage

@RunWith(AndroidJUnit4::class)
class TestDialogDisplays {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkShowDialogMessage() {
        with(FirstPage()) {
            pressShowDialogButton()
            checkTextOfTitleDialogDisplay(textOfTitle)
            checkTextOfAnnotationDialogDisplay(textOfAnnotation)
        }
    }
    @Test
    fun checkDialogScreenClosed() {
        checkShowDialogMessage()
        with(FirstPage()) {
            pressBackButton()
            checkFirstTextDisplay(firstText)
        }
    }
}

