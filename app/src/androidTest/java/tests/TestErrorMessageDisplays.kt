package tests

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.pages.FirstPage
import ru.tinkoff.myupgradeapplication.pages.LoginPage

@RunWith(AndroidJUnit4::class)
class TestErrorMessageDisplays {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkEmptyPasswordFieldErrorMessageDisplay() {
        with(FirstPage()) {
            pressNextButton()
        }
        with(LoginPage()) {
            checkLoginPageDisplay()
            enterLogin(loginValue)
            pressSubmitButton()
            checkTextErrorMessagePasswordDisplay(textErrorPassword)
        }
    }

    @Test
    fun checkEmptyLoginFieldErrorMessageDisplay() {
        with(FirstPage()) {
            pressNextButton()
        }
        with(LoginPage()) {
            checkLoginPageDisplay()
            enterPassword(passwordValue)
            pressSubmitButton()
            checkTextErrorMessageLoginDisplay(textErrorLogin)
        }
    }

    @Test
    fun checkEmptyLoginAndPasswordFieldErrorMessageDisplay() {
        with(FirstPage()) {
            pressNextButton()
        }
        with(LoginPage()) {
            checkLoginPageDisplay()
            pressSubmitButton()
            checkTextErrorMessageLoginAndPasswordDisplay(textNoLoginAndPassword)
        }
    }
}

