package ru.tinkoff.myupgradeapplication.espressoTests.PO.pages

import androidx.appcompat.widget.AppCompatTextView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textview.MaterialTextView
import io.github.kakaocup.kakao.text.KTextView
import org.hamcrest.Matchers
import org.hamcrest.Matchers.allOf
import ru.tinkoff.myupgradeapplication.R

class EspressoFirstScreen {
    val firstButtonMatcher = withId(R.id.button_first)
    val showDialogButtonMatcher = withId(R.id.dialog_button)
    val changeButtonMatcher = withId(R.id.change_button)
    val screenTitleMatcher = allOf(
        Matchers.instanceOf(AppCompatTextView::class.java),
        ViewMatchers.withParent(Matchers.instanceOf(MaterialToolbar::class.java))
    )
    val showDialogTextMatcher = withText("Теперь ты автоматизатор")
    val showDialogText = "Теперь ты автоматизатор"
    val firstText =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.first_text)
    val secondText =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.second_text)
    val textOnScreen = KTextView {
        isInstanceOf(MaterialTextView::class.java)
    }

    fun clickFirstButton() {
        onView(firstButtonMatcher)
            .perform(click())
    }

    fun clickChangeButton() {
        onView(changeButtonMatcher)
            .perform(click())
    }

    fun clickShowDialogButton() {
        onView(showDialogButtonMatcher)
            .perform(click())
    }

    fun checkScreenTitle(title: String) {
        onView(screenTitleMatcher)
            .check(matches(withText(title)))
    }

    fun checkTextShowDialogScreen(showDialogText: String) {
        onView(showDialogTextMatcher)
            .check(matches(withText(showDialogText)))
    }

    fun checkTextOnScreen(text: String) {
        textOnScreen {
            isDisplayed()
            hasText(text)
        }
    }
}

