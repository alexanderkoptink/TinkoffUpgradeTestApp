package ru.tinkoff.myupgradeapplication.espressoTests.PO.tests

import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.espressoTests.PO.pages.EspressoFirstScreen
import ru.tinkoff.myupgradeapplication.espressoTests.PO.pages.EspressoLoginScreen

@RunWith(AndroidJUnit4::class)
class EspressoTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkEmptyPasswordFieldErrorMessageDisplayEspresso() {
        EspressoFirstScreen().clickFirstButton()
        with(EspressoLoginScreen()) {
            typeTextToLoginFiled(loginValue)
            clickSubmitButton()
            checkPasswordHintColor(R.color.error_hint)
        }
    }

    @Test
    fun checkEmptyLoginFieldErrorMessageDisplayEspresso() {
        EspressoFirstScreen().clickFirstButton()
        with(EspressoLoginScreen()) {
            typeTextToPasswordFiled(passwordValue)
            clickSubmitButton()
            checkLoginHintColor(R.color.error_hint)
        }
    }

    @Test
    fun checkEmptyLoginAndPasswordFieldErrorMessageDisplayEspresso() {
        EspressoFirstScreen().clickFirstButton()
        with(EspressoLoginScreen()) {
            clickSubmitButton()
            checkLoginHintColor(R.color.error_hint)
            checkPasswordHintColor(R.color.error_hint)
        }
    }

    @Test
    fun checkShowDialogMessageEspresso() {
        with(EspressoFirstScreen()) {
            clickShowDialogButton()
            checkTextShowDialogScreen(showDialogText)
        }
    }

    @Test
    fun checkDialogScreenClosedEspresso() {
        with(EspressoFirstScreen()) {
            clickShowDialogButton()
            pressBack()
            checkScreenTitle("First Fragment")
        }
    }

    @Test
    fun checkTextOnFirstScreenWasChangedAfterClickOnChangeButtonEspresso() {
        with(EspressoFirstScreen()) {
            checkTextOnScreen(firstText)
            clickChangeButton()
            checkTextOnScreen(secondText)
            clickFirstButton()
            pressBack()
            checkTextOnScreen(firstText)
        }
    }

    @Test
    fun checkLoginAndPasswordNotSavedAfterBackScreenEspresso() {
        with(EspressoFirstScreen()) {
            clickFirstButton()
            with(EspressoLoginScreen()) {
                typeTextToLoginFiled(loginValue)
                typeTextToPasswordFiled(passwordValue)
                clickPreviousButton()
                with(EspressoFirstScreen()) {
                    clickFirstButton()
                }
                with(EspressoLoginScreen()) {
                    clickSubmitButton()
                    checkLoginHintColor(R.color.error_hint)
                    checkPasswordHintColor(R.color.error_hint)
                }
            }
        }
    }
}

