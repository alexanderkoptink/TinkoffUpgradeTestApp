package ru.tinkoff.myupgradeapplication.kaspressoTests.PO.tests.pages

import androidx.appcompat.widget.AppCompatTextView
import androidx.test.platform.app.InstrumentationRegistry
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textview.MaterialTextView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.myupgradeapplication.R

class KaspressoFirstScreen : BaseScreen() {
    val nextButton = KButton { withId(R.id.button_first) }
    val changeButton = KButton { withId(R.id.change_button) }
    val showDialogButton = KButton { withId(R.id.dialog_button) }
    val screenTitle = KTextView {
        isInstanceOf(AppCompatTextView::class.java)
        withParent { isInstanceOf(MaterialToolbar::class.java) }
    }
    val textOnScreen = KTextView {
        isInstanceOf(MaterialTextView::class.java)
    }
    val showDialogText = "Теперь ты автоматизатор"
    val firstText =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.first_text)
    val secondText =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.second_text)


    fun checkTextOnScreen(text: String) {
        textOnScreen {
            isDisplayed()
            hasText(text)
        }
    }

    companion object {
        inline operator fun invoke(crossinline block: KaspressoFirstScreen.() -> Unit) =
            KaspressoFirstScreen().block()
    }
}

