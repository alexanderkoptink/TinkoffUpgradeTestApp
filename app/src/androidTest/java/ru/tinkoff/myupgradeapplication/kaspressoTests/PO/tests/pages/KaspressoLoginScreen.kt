package ru.tinkoff.myupgradeapplication.kaspressoTests.PO.tests.pages

import com.google.android.material.snackbar.SnackbarContentLayout
import com.google.android.material.textview.MaterialTextView
import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.matchers.TextViewHintColorMatcher

class KaspressoLoginScreen : BaseScreen() {

    val editTextlogin = KEditText { withId(R.id.edittext_login) }
    val editTextpassword = KEditText { withId(R.id.edittext_password) }
    val buttonSubmit = KButton { withId(R.id.button_submit) }
    val buttonPrevious = KButton { withId(R.id.button_second) }
    val textOnSnackBar = KTextView {
        isInstanceOf(MaterialTextView::class.java)
        withParent { isInstanceOf(SnackbarContentLayout::class.java) }
    }
    val loginValue = "Tinkoff"
    val passwordValue = "Upgrade"
    val errorMessageLogin = "Login field must be filled!"
    val errorMessagePassword = "Password field must be filled!"
    val errorMessageLoginAndPassword = "Both of fields must be filled!"
    fun checkLoginFieldHintColor(color: Int) {
        editTextlogin.matches { TextViewHintColorMatcher(color) }
    }

    fun checkPasswordFieldHintColor(color: Int) {
        editTextpassword.matches { TextViewHintColorMatcher(color) }
    }

    fun checkLoginFieldErrorState() {
        checkLoginFieldHintColor(R.color.error_hint)
    }

    fun checkPasswordFieldErrorState() {
        checkPasswordFieldHintColor(R.color.error_hint)
    }

    fun typeTextToLoginFiled(text: String) {
        editTextlogin.typeText(text)
    }

    fun typeTextToPasswordFiled(text: String) {
        editTextpassword.typeText(text)
    }

    companion object {
        inline operator fun invoke(crossinline block: KaspressoLoginScreen.() -> Unit) =
            KaspressoLoginScreen().block()
    }
}

