package ru.tinkoff.myupgradeapplication.kaspressoTests.PO.pages.tests

import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Rule
import org.junit.Test
import ru.tinkoff.myupgradeapplication.MainActivity
import ru.tinkoff.myupgradeapplication.R
import ru.tinkoff.myupgradeapplication.kaspressoTests.PO.tests.pages.KaspressoFirstScreen
import ru.tinkoff.myupgradeapplication.kaspressoTests.PO.tests.pages.KaspressoLoginScreen
import ru.tinkoff.myupgradeapplication.matchers.kaspressoBuilder

class KaspressoTest : TestCase(kaspressoBuilder) {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkEmptyPasswordFieldErrorMessageDisplayKaspressoTest() =
        run {
            KaspressoFirstScreen {
                nextButton.click()
            }
            KaspressoLoginScreen {
                typeTextToLoginFiled(loginValue)
                buttonSubmit.click()
                checkPasswordFieldErrorState()
                textOnSnackBar.hasText(errorMessagePassword)
            }
        }

    @Test
    fun checkEmptyLoginFieldErrorMessageDisplayKaspressoTest() =
        run {
            step("Click on next button") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Check error message of login on snackbar") {
                KaspressoLoginScreen {
                    KaspressoLoginScreen {
                        typeTextToPasswordFiled(passwordValue)
                        buttonSubmit.click()
                        checkLoginFieldErrorState()
                        textOnSnackBar.hasText(errorMessageLogin)
                    }
                }
            }
        }

    @Test
    fun checkEmptyLoginAndPasswordFieldErrorMessageDisplayKaspressoTest() =
        run {
            step("Click on next button") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Check error message of password on snackbar") {
                KaspressoLoginScreen {
                    buttonSubmit.click()
                    checkLoginFieldErrorState()
                    checkPasswordFieldErrorState()
                    textOnSnackBar.hasText(errorMessageLoginAndPassword)
                }
            }
        }

    @Test
    fun checkShowDialogScreenOpenedKaspressoTest() =
        run {
            step("Click on showDialog button") {
                KaspressoFirstScreen {
                    showDialogButton.click()
                }
            }
            step("Check dialog screen is opened") {
                KaspressoFirstScreen {
                    checkTextOnScreen(showDialogText)
                }
            }
        }

    @Test
    fun checkShowDialogScreenClosedKaspressoTest() =
        run {
            step("Click on showDialog button") {
                KaspressoFirstScreen {
                    showDialogButton.click()
                }
            }
            step("Check dialog screen is opened") {
                KaspressoFirstScreen {
                    checkTextOnScreen(showDialogText)
                }
            }
            step("Click on Back button") {
                KaspressoFirstScreen {
                    pressBack()
                }
            }
            step("Check dialog screen is closed") {
                KaspressoFirstScreen {
                    screenTitle.isDisplayed()
                }
            }
        }

    @Test
    fun checkTextPositionNotSavedOnNavigationKaspressoTest() =
        run {
            step("Check first text on screen") {
                KaspressoFirstScreen {
                    checkTextOnScreen(firstText)
                }
            }
            step("Click on Change button") {
                KaspressoFirstScreen {
                    changeButton.click()
                }
            }
            step("Check second text on screen") {
                KaspressoFirstScreen {
                    checkTextOnScreen(secondText)
                }
            }
            step("Click on Next button") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Click on Previous button") {
                KaspressoLoginScreen {
                    buttonPrevious.click()
                }
            }
            step("Check first text on screen") {
                KaspressoFirstScreen {
                    checkTextOnScreen(firstText)
                }
            }
        }

    @Test
    fun checkLoginAndPasswordNotSavedAfterBackScreenKaspressoTest() =
        run {
            step("Click on next button") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Enter login and password") {
                KaspressoLoginScreen {
                    typeTextToLoginFiled(loginValue)
                    typeTextToPasswordFiled(passwordValue)
                }
            }
            step("Click on Back button") {
                KaspressoLoginScreen {
                    buttonPrevious.click()
                }
            }
            step("Click on next button") {
                KaspressoFirstScreen {
                    nextButton.click()
                }
            }
            step("Check login and password fields are empty") {
                KaspressoLoginScreen {
                    editTextlogin.hasEmptyText()
                    editTextpassword.hasEmptyText()
                    buttonSubmit.click()
                    textOnSnackBar.hasText(errorMessageLoginAndPassword)
                    checkLoginFieldHintColor(R.color.error_hint)
                    checkPasswordFieldHintColor(R.color.error_hint)
                }
            }
        }
}

