package ru.tinkoff.myupgradeapplication.pages

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice

open class BasePage {
    val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    val waitingTimeOut = 3000L
    fun pressBackButton()
    {
        device.pressBack()
    }
}

