package ru.tinkoff.myupgradeapplication.pages

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until
import ru.tinkoff.myupgradeapplication.R

class FirstPage : BasePage() {
    val nextButtonSelector = By.res("ru.tinkoff.myupgradeapplication:id/button_first")
    val showDialogButtonSelector = By.res("ru.tinkoff.myupgradeapplication:id/dialog_button")
    val changeButtonSelector = By.res("ru.tinkoff.myupgradeapplication:id/change_button")
    val textOfTitle = "Важное сообщение"
    val textOfAnnotation = "Теперь ты автоматизатор"
    val firstText =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.first_text)
    val secondText =
        InstrumentationRegistry.getInstrumentation().targetContext.getString(R.string.second_text)

    fun pressNextButton() {
        device
            .wait(Until.findObject(nextButtonSelector), waitingTimeOut)
            .click()
    }

    fun pressShowDialogButton() {
        device
            .wait(Until.findObject(showDialogButtonSelector), waitingTimeOut)
            .click()
    }

    fun pressChangeButton() {
        device
            .wait(Until.findObject(changeButtonSelector), waitingTimeOut)
            .click()
    }

    fun checkTextOfTitleDialogDisplay(checkTextOfTitleDialogDisplay: String) {
        assert(
            device
                .wait(Until.hasObject(By.text(checkTextOfTitleDialogDisplay)), waitingTimeOut)
        )
    }

    fun checkTextOfAnnotationDialogDisplay(checkTextOfAnnotationDialogDisplay: String) {
        assert(
            device
                .wait(Until.hasObject(By.text(checkTextOfAnnotationDialogDisplay)), waitingTimeOut)
        )
    }

    fun checkFirstTextDisplay(firstTextLine: String) {
        assert(
            device
                .wait(Until.hasObject(By.textContains(firstTextLine)), waitingTimeOut)
        )

    }

    fun checkSecondTextDisplay(secondTextLine: String) {
        assert(
            device
                .wait(Until.hasObject(By.textContains(secondTextLine)), waitingTimeOut)
        )

    }
}

