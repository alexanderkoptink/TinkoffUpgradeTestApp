package ru.tinkoff.myupgradeapplication.pages

import androidx.test.uiautomator.By
import androidx.test.uiautomator.Until

class LoginPage : BasePage() {
    val loginFieldSelector = By.res("ru.tinkoff.myupgradeapplication:id/edittext_login")
    val passwordFieldSelector = By.res("ru.tinkoff.myupgradeapplication:id/edittext_password")
    val submitButtonSelector = By.res("ru.tinkoff.myupgradeapplication:id/button_submit")
    val previousButton = By.res("ru.tinkoff.myupgradeapplication:id/button_second")
    val loginValue = "admin"
    val textErrorPassword = "Password field must be filled!"
    val passwordValue = "password"
    val textErrorLogin = "Login field must be filled!"
    val textNoLoginAndPassword = "Both of fields must be filled!"
    fun checkLoginPageDisplay() {
        assert(
            device
                .wait(Until.hasObject(loginFieldSelector), waitingTimeOut)
        )
    }

    fun checkLoginAndPasswordCleared(textNoLoginAndPassword: String) {
        assert(
            device
                .wait(Until.hasObject(By.text(textNoLoginAndPassword)), waitingTimeOut)
            )
    }

    fun pressPreviousButton() {
        device
            .wait(Until.findObject(previousButton), waitingTimeOut)
            .click()
    }

    fun pressSubmitButton() {
        device
            .wait(Until.findObject(submitButtonSelector), waitingTimeOut)
            .click()
    }

    fun enterLogin(loginValue: String) {
        device
            .wait(Until.findObject(loginFieldSelector), waitingTimeOut)
            .text = loginValue
    }

    fun checkTextErrorMessagePasswordDisplay(passwordErrorMessageDisplay: String) {
        assert(
            device
                .wait(Until.hasObject(By.text(passwordErrorMessageDisplay)), waitingTimeOut)
        )
    }

    fun enterPassword(passwordValue: String) {
        device
            .wait(Until.findObject(passwordFieldSelector), waitingTimeOut)
            .text = passwordValue
    }

    fun checkTextErrorMessageLoginDisplay(loginErrorMessageDisplay: String) {
        assert(
            device
                .wait(Until.hasObject(By.text(loginErrorMessageDisplay)), waitingTimeOut)
        )
    }

    fun checkTextErrorMessageLoginAndPasswordDisplay(loginAndPasswordErrorMessageDisplay: String) {
        assert(
            device
                .wait(Until.hasObject(By.text(loginAndPasswordErrorMessageDisplay)), waitingTimeOut)
        )
    }
}

